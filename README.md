1. Assassins Blade: Buff that poisons your sword, causing it to inflict a damage over time poison on the target (5 Seconds). (10% Chance Happening)

2. Backstab: Passive buff that causes you to deal more damage to a target when you strike them from behind.

3. Bleed: Inflicts Wither. (10 seconds) (10% Chance Happening)

4. Bolt: Summons a blot of lightning down on the target. (15% Chance Happening)

5. Charge: Charges you towards the target.

6. Escape Artist: Removes any efects affecting your movement.

7. Fireball: Shoots a fireball that ignites the target.

8. Gills: Allows you to breath underwater for a short time.(1min)

9. IceArrow: Causes the next arrow you shoot to deal ice damage and slow the target that it hits.(10 seconds) (10% Chance Happening)

10. Invuln: Makes you temporarily immune to all damage. (15seconds) (5% Chance Happening)


11. Might: Boosts your damage with all weapons for a certain amount of time. (10seconds) (10% Chance Happening)

12. One: Provides a short term movement speed boost.(20seconds) (20% Chance Happening)

13. Poison: Inflicts a poison damage over time effect on the target.(15seconds) (20% Chance Happening)

14. PoisonArrow: Causes the next arrow you shoot to poison the target it hits. (10 seconds) (15% Chance Happening)

15. Root: Immobilizes the target for several seconds. (25% Chance Happening)

16. Safefall: Allows a short period where fall damage is negated. (30% Chance Happening)

17. Slow: Causes the target to move slower for a time.(15 seconds) (15% Chance Happening)

18. Smite: Strikes the player with damage from range.

19. Smoke: Causes you to disappear completely from view for a time, dealing/taking damage ends the effect.(20 seconds) (15% Chance Happening)

20. Sneak: Allows for sneaking at full movement speed.

21. SuperJump: Launches you into the air, and gives you short term safefall. (30% Chance Happening)

22. Telekinesis: Allows long range use of buttons and levers.

23. Web: Catches your target in web blocks. (10% Chance Happening) (webs despawn after 10 seconds)

24. Wolf: Allow you to summon a wolf to help u in pvp. (whenever wanted)